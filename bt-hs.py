#!/usr/bin/env python2

import dbus
from dbus.mainloop.glib import DBusGMainLoop
import gobject

import time
from datetime import datetime, timedelta

import subprocess

# ID of the device we care about
DEV_ID = 'E4:22:A5:10:D4:58'.replace(":", "_")

dbus_loop = DBusGMainLoop()
bus = dbus.SystemBus(mainloop=dbus_loop)

# Figure out the path to the headset
print('/org/bluez/hci0/dev_' + DEV_ID)
headset = bus.get_object('org.bluez',  '/org/bluez/hci0/dev_' + DEV_ID)
# ^^^ I'm not sure if that's kosher. But it works.

conn_time = datetime.now()

def cb(*args, **kwargs):
    global conn_time
    is_connected = args[1]['Connected']
    if isinstance(is_connected, dbus.Boolean) and is_connected:
        print("Connected")
        conn_time = datetime.now()
    elif isinstance(is_connected, dbus.Boolean) and not is_connected:
        print("Disconnected")
        nu = datetime.now()
        diff = nu - conn_time
        if diff < timedelta(seconds=5):
            time.sleep(5)
            headset.get_dbus_method('Connect', 'org.bluez.Device1')()

headset.connect_to_signal("PropertiesChanged", cb, interface_keyword='iface', member_keyword='mbr', path_keyword='path')

loop = gobject.MainLoop()
loop.run()
