#!/usr/bin/env bash

. dl.list

IFS=" "
for line in "${list[@]}"; do
	while read ep url; do
		wget -cO "ep${ep}_720p.mp4" "${url}"
	done <<< "${line}"
done
