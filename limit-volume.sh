#!/usr/bin/env bash

[ $(pactl list sinks | awk '/Volume: front-left/ { print $5 }' | cut -d% -f1) -gt 100 ] && \
	pactl set-sink-volume @DEFAULT_SINK@ 100%
