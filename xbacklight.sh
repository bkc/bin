#!/usr/bin/env bash

CUR=$(echo "scale=0; ($(xbacklight -get) + 0.5) / 1.0" | bc) # get the rounded backlight
STEPS=(0 1 2 4 6 10 16 25 40 63 100 ) # log-curve from 0->100

_found=0
# see if it's even there...
for i in ${STEPS[@]}; do
	if [ $i -eq ${CUR} ]; then
		_found=1
	fi
done

# if not found, find closest higher...
if [ $_found -eq 0 ]; then
	for i in ${!STEPS[*]}; do
		if [ ${STEPS[$i]} -gt ${CUR} ]; then
			xbacklight -set ${STEPS[$i]}
			CUR=$(echo "scale=0; ($(xbacklight -get) + 0.5) / 1.0" | bc) # get the rounded backlight
			break
		fi
	done
fi

# iterate through and set to next neighbour
for i in ${!STEPS[*]}; do
	if [ ${STEPS[$i]} -eq $CUR ]; then
		if [ "$1" == "down" ] ; then
			[[ $i -gt 0 ]] && xbacklight -set ${STEPS[$i-1]}
		else
			[[ $i -lt $((${#STEPS[@]} -1)) ]] && xbacklight -set ${STEPS[$i+1]}
		fi
	fi
done
