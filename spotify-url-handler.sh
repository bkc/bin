#!/usr/bin/env bash

if [[ "$1" == *"open.spotify.com"* ]]; then
	# Strip out useless query arguments
	spotify_url=$(echo "$1" | sed -e 's#\?.*##g')
	# Extract the Spotify URL and convert it to a Spotify URI
	spotify_uri=$(echo "$spotify_url" | sed -e 's#https://open.spotify.com/#spotify:#' -e 's#/#:#g')

	# Launch Spotify with the converted URI
	exec $(which spotify) --uri="$spotify_uri"
else
	exec firefox "$1"
fi
