#!/usr/bin/env bash
set -x
X_USER=$(w -h -s | awk '/:[0-9]\W/ {print $1}' | head -1)

# no display?
[[ "$X_USER" == "" ]] && exit 1

# run this as a display-user
if [[ "${X_USER}" != "$(whoami)" ]]; then
	/usr/bin/su $X_USER -m -c "$0 ${@}"
fi


#export DISPLAY=$(w -h -s | grep ":[0-9]\W" | head -1 | awk '{print $8}')
export DISPLAY=":0"

export XAUTHORITY="/home/$X_USER/.Xauthority"

xinput list --name-only | grep --quiet "Varmilo Keyboard" >/dev/null
if [ $? -eq 0 ]; then
	# The keyboard is plugged in. Load keymap
	[[ -x "/home/$X_USER/.setxkbmap" ]] && /home/$X_USER/.setxkbmap
fi
