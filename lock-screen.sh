#!/usr/bin/env bash

DISPLAY=:0
#FP="$(mktemp '/tmp/lock-screen.XXXX.png')"
#scrot -e "mogrify -scale 10% -scale 1000% \$f" "$FP"
#i3lock -nefi "$FP"
#rm $FP
revert() {
	xset dpms 0 0 0
}
trap revert HUP INT TERM
xset +dpms dpms 30 30 30
i3lock --nofork --show-failed-attempts --color 5b0a91 #--image /home/bkc/.config/i3/lock.png -t
revert
