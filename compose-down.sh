#!/usr/bin/env bash

for cfg in $(docker compose ls --format json | jq -r '.[].ConfigFiles | split(",";"") | .[0]'); do
	echo $(dirname $cfg)
	pushd $(dirname $cfg)
	docker compose down
	popd
done
