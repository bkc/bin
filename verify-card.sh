#!/usr/bin/env bash

PUBKEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxyy8c2plcQZYUY1N8EOM833gkpqR0oUBeuXLGs1bGabTW0ApLSCP58WiSIff1nFc5oXRXEovpj6hceh4uTwkuv/FxI02vhNBX18neGMvmwPDO2tiIB1P/wGxffSLY+N1WOmZq7s75YtSDnTMxcx4rNwFg48Tt80gETEflH3LRPA4K1CjKO8bwh6ZvycUuPpt7NHRE/ZhZcVEOGVJslRKRaofyzGHqArdk824Mhhd3Cdw5Yor60VE3c4DftRmWL+vYaKpFe6suLTO282WbBIFPnfUyy78n+4N3MVP4sttjY/mLLGD2HdmDKTfHxZJB7bIq/XFp5oJGhhbWyyrLqpUtQIDAQAB"

## pkcs11-tool is slower than pkcs15-tool for some reason
#CARDKEY="$(pkcs11-tool --read-object --id 10 --type pubkey | base64 | tr -d '\n')"
CARDKEY="$(pkcs15-tool --read-public-key 10 | sed "/^--*.*$/d" | tr -d '\n')"

if [ "${PUBKEY}" != "${CARDKEY}" ]; then
	exit 1
fi
